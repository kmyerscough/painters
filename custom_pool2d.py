import keras
import keras.backend as K
from keras.utils import conv_utils
from keras.engine import InputSpec
from keras.engine import Layer

class CustomPool2D(Layer):
    """Max pooling operation for spatial data.

    # Arguments
        output_size: Size of output layer width and height
        padding: One of `"valid"` or `"same"` (case-insensitive).
        fit: One of 'snug', 'inside', 'pad'
        data_format: A string,
            one of `channels_last` (default) or `channels_first`.
            The ordering of the dimensions in the inputs.
            `channels_last` corresponds to inputs with shape
            `(batch, height, width, channels)` while `channels_first`
            corresponds to inputs with shape
            `(batch, channels, height, width)`.
            It defaults to the `image_data_format` value found in your
            Keras config file at `~/.keras/keras.json`.
            If you never set it, then it will be "channels_last".

    # Input shape
        - If `data_format='channels_last'`:
            4D tensor with shape:
            `(batch_size, rows, cols, channels)`
        - If `data_format='channels_first'`:
            4D tensor with shape:
            `(batch_size, channels, rows, cols)`

    # Output shape
        - If `data_format='channels_last'`:
            4D tensor with shape:
            `(batch_size, pooled_rows, pooled_cols, channels)`
        - If `data_format='channels_first'`:
            4D tensor with shape:
            `(batch_size, channels, pooled_rows, pooled_cols)`
    """
    def __init__(self, output_dim=(1, 1), padding='valid', fit='snug',
                 data_format=None, **kwargs):
        super(CustomPool2D, self).__init__(**kwargs)
        data_format = conv_utils.normalize_data_format(data_format)
        self.output_dim = conv_utils.normalize_tuple(output_dim, 2, 'output_dim')
        self.padding = conv_utils.normalize_padding(padding)
        self.fit = fit
        self.data_format = conv_utils.normalize_data_format(data_format)
        self.input_spec = InputSpec(ndim=4)

    def build(self, input_shape):
        self.input_spec = [InputSpec(shape=input_shape)]

    def compute_strides_and_pool_size(self, input_shape):
        if self.data_format == 'channels_first':
            w, h = input_shape[2:4]
        elif self.data_format == 'channels_last':
            w, h = input_shape[1:3]

        if self.fit == 'snug': # in this case pooling should
            strides = w // self.output_dim[0], h // self.output_dim[1]
            pool_size = w - strides[0]*self.output_dim[0], h - strides[1]*self.output_dim[1]
        elif self.fit == 'inside':
            strides = w // self.output_dim[0], h // self.output_dim[1]
            pool_size = strides.copy()
        elif self.fit == 'pad':
            strides = (w - 1) // self.output_dim[0] + 1, (h - 1) // self.output_dim[1] + 1
            pool_size = strides.copy()
        else:
            raise NotImplemented

        print w, h
        return strides, pool_size

    def compute_output_shape(self, input_shape):
        if self.data_format == 'channels_first':
            return (input_shape[0], input_shape[1], self.output_dim[0], self.output_dim[1])
        elif self.data_format == 'channels_last':
            return (input_shape[0], self.output_dim[0], self.output_dim[1], input_shape[3])

    def _pooling_function(self, inputs, pool_size, strides,
                          padding, data_format):
        output = K.pool2d(inputs, pool_size, strides,
                          padding, data_format,
                          pool_mode='max')
        return output

    def call(self, inputs):
        strides, pool_size = self.compute_strides_and_pool_size(inputs.shape)
        output = self._pooling_function(inputs=inputs,
                                        pool_size=pool_size,
                                        strides=strides,
                                        padding=self.padding,
                                        data_format=self.data_format)
        return output

    def get_config(self):
        config = {'output_dim': self.output_dim,
                  'padding': self.padding,
                  'data_format': self.data_format}
        base_config = super(CustomPool2D, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))