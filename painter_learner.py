import numpy as np
import keras
from keras import backend as K
from custom_pool2d import CustomPool2D
from resize_images import ResizeImages
import pandas as pd
from PIL import Image
import os
from keras.optimizers import RMSprop # not sure what optimizer to use...

"""
Beware that pandas slicing includes both the start AND end! i.e. 3:5 has length 3.
"""

class PainterLearner(object):
    def __init__(self, max_batch_size=1, clip_factor=1):
        """
        :param max_batch_size: how large a batch to return
        :param clip_factor: how much to clip images
        """
        self.max_batch_size = max_batch_size
        self.clip_factor = clip_factor
        self.data_dir = '.'
        df = pd.read_csv(os.path.join(self.data_dir, 'train_info.csv'))
        self.dataframe = df

        self.N_genres = df['genre'].nunique()
        self.N_styles = df['style'].nunique()
        self.N_trainval = df.shape[0]
        self.N_train = self.N_trainval*4/5
        self.N_val = self.N_trainval - self.N_train

        self.one_hot_dataframe = self.set_one_hot_dataframe()
        self.one_hot_grouped_dataframe_train = self.set_one_hot_grouped_dataframe(self.one_hot_dataframe[:self.N_train])
        self.one_hot_grouped_dataframe_val   = self.set_one_hot_grouped_dataframe(self.one_hot_dataframe[self.N_train:])
        self.one_hot_grouped_dataframe = self.set_one_hot_grouped_dataframe(self.one_hot_dataframe)

        self.N_groups = len(self.one_hot_grouped_dataframe_train)
        self.N_batches = ((self.one_hot_grouped_dataframe_train.size() - 1) // self.max_batch_size + 1).sum()

        """
        Currently batch_size is fixed at 1, because we cannot join multiple paintings of different sizes.
        By using df.group_by(['pixelsx', 'pixelsy']) this may be fixed to allow batches where the paintings are 
        (roughly?) the same size.
        """
        self.train_generator = self._generator(type='train')
        self.val_generator = self._generator(type='train')

    def create_model(self, uniform_size = (16, 16)):

        def _conv(N_out, layer_in, name=None):
            return keras.layers.convolutional.Conv2D(N_out, (3, 3), strides=(1, 1), padding='same', data_format=None,
                                            dilation_rate=(1, 1), activation='relu', use_bias=True, name=name)(layer_in)

        def _pool(layer_in, name=None):
            return keras.layers.pooling.MaxPooling2D(pool_size=(2, 2), padding='same', name=name)(layer_in)

        """
        Start with the input. The shape of the imput will vary per image.
        But the characteristics of a painting depend both on fine level 
        stuff (such as brush strokes) which would be lost by starting with 
        an (anisotropic) rescaling. Therefore we start with some convolution on the original image
        """
        input = keras.Input(shape=(None, None, 3), name='input')

        # some convolution layers for the local features
        conv1 = _conv(11, input, name='conv1')
        pool1 = _pool(conv1, name='pool1')

        conv2 = _conv(22, pool1, name='conv2')
        pool2 = _pool(conv2, name='pool2')

        conv3 = _conv(44, pool2, name='conv3')

        # scale the convolved layers to a uniform size, needed to get a uniform output
        uniform_size_conv = ResizeImages(output_dim=uniform_size, name='uniform_size_conv')(conv3) # possibly an upsampling!
        # also scale the input image
        uniform_size_input = ResizeImages(output_dim=uniform_size, name='uniform_size_input')(input)

        # concatenate the two resized layers (cheaper than letting the convolution layers copy the image)
        uniform_size_combined = keras.layers.Concatenate()([uniform_size_conv, uniform_size_input])

        conv4 = _conv(88, uniform_size_combined, name='conv4')
        pool4 = _pool(conv4, name='pool4')

        conv5 = _conv(2*88, pool4, name='conv5')
        pool5 = _pool(conv5, name='pool5')

        conv6 = _conv(4*88, pool5, name='conv6')
        pool6 = _pool(conv6, name='pool6')

        conv7 = _conv(8*88, pool6, name='conv7')
        pool7 = _pool(conv7, name='pool7')

        fc1 = keras.layers.Dense(self.N_genres * self.N_styles, name='fc1', activation='relu')(pool7)

        out_genres = keras.layers.Dense(self.N_genres, name='genres', activation='sigmoid')(fc1)
        out_styles = keras.layers.Dense(self.N_styles, name='styles', activation='sigmoid')(fc1)

        model = keras.models.Model(inputs=input, outputs=[out_genres, out_styles])

        # not sure what optimizer to use...
        rmsprop = RMSprop(lr=0.0001)
        model.compile(loss={'genres': 'categorical_crossentropy', 'styles': 'categorical_crossentropy'},
                      loss_weights={'genres': 1., 'styles': 1.},
                      optimizer=rmsprop,
                      metrics=['accuracy'])

        return model

    def _generator(self, type='train'):
        """
        Produces X,y output from the dataframe iterator
        :param type: whether to iterate over the train or val sets, or both
        :return: an X (image) and y (tuple of label one-hot vectors) from a dataframe
        """
        iterator = self._df_iterator(type=type)
        while True:
            df = iterator.next()
            yield self._get_X_and_y(df)

    def _df_iterator(self, type='train'):
        """
        Iterates over (part of) the dataframe, returning chunks of the dataframe
        :param type: whether to iterate over the train or val sets, or both
        :return: a dataframe of maximally self.max_batch_size long
        """

        if type == 'train':
            df = self.one_hot_grouped_dataframe_train
        elif type == 'val':
            df = self.one_hot_grouped_dataframe_val
        elif type == 'all' or type == 'trainval':
            df = self.one_hot_grouped_dataframe
        else:
            raise NotImplementedError

        sample_df = df
        while True:
            # sample_df = sample_df.sample(frac=1) # sample the grouped array # does not work...
            for name, group in sample_df:
                N_group = len(group)
                if N_group > 1:
                    # shuffle the df
                    sample_df = group.sample(frac=1)
                    # loop over batches of the inner array
                    grouper = np.arange(N_group)  // self.max_batch_size
                    for name_inner, group_inner in sample_df.groupby(grouper):
                        yield group_inner
                else:
                    # no bother going through all the above for the (many) groups of length 1
                    yield group

    def set_one_hot_dataframe(self):
        """
        :return: a "one-hot" dataframe, i.e. one where the class info on genre and style has been replaced by a series 
        of dummy variables. This dataframe also contains the pixel dimensions.
        """
        print "Making a one-hot dataframe"
        keep_list = ['filename', 'artist', 'genre', 'style']

        dataframe_all = pd.read_csv(os.path.join(self.data_dir, 'all_data_info.csv'))
        dataframe_pixels = dataframe_all[['pixelsx', 'pixelsy', 'new_filename']]
        dataframe_pixels = dataframe_pixels.rename(columns={'new_filename': 'filename'})

        dataframe_pixels[['pixelsx', 'pixelsy']] = dataframe_pixels[['pixelsx', 'pixelsy']].astype(int)
        dataframe_train = pd.get_dummies(self.dataframe[keep_list], columns=['genre', 'style'])

        # df = pd.concat([dataframe_train, dataframe_pixels], axis=1, join='inner')
        df = pd.merge(dataframe_train, dataframe_pixels, on='filename')
        return df

    def set_one_hot_grouped_dataframe(self, df):
        """
        :param df: a dataframe from which to make the one_hot_grouped 
        :return: a dataframe with all the paintings grouped by size (both x and y must match). The paintings are 
         clipoed such that the dimensions are divisible by self.clip_factor, resulting in larger groups allowing larger 
         batches to be performed in each iteration. 
        """
        df = df.copy()
        df['pixelsx'] = (df['pixelsx'] // self.clip_factor) * self.clip_factor
        df['pixelsy'] = (df['pixelsy'] // self.clip_factor) * self.clip_factor
        grouped = df.groupby(['pixelsx', 'pixelsy'])
        return grouped

    def _get_X_and_y(self, df):
        """
        :param df: dataframe (with length of batch_size) 
        :return: an image array X and a dict of arrays for genre and style
        """
        # print df['filename']
        w = (df['pixelsx'].iloc[0] // self.clip_factor) * self.clip_factor
        h = (df['pixelsy'].iloc[0] // self.clip_factor) * self.clip_factor
        if abs(h - df['pixelsy'].iloc[0]) > self.clip_factor or abs(w - df['pixelsx'].iloc[0]) > self.clip_factor:
            print "w, h: {}, {}; x, y: {}, {}".format(w, h, df['pixelsy'].iloc[0], df['pixelsy'].iloc[0])
        X = np.zeros((df.shape[0], h, w, 3)) # channels_last ordering
        # print 'X.shape:', X.shape
        # print "y['genres'].shape:", y['genres'].shape
        # print "y['styles'].shape:", y['styles'].shape
        for filename, X_im in zip(df['filename'], X):
            with Image.open(os.path.join(self.data_dir, 'train', filename)) as im:
                try:
                    X_im[:, :, :] = np.array(im)[:h, :w, :]
                except ValueError:
                    im_np = np.array(im)
                    df_shape = self.one_hot_dataframe.loc[self.one_hot_dataframe['filename'] == filename][['pixelsy', 'pixelsx']]
#                     print "Not managed to fit image {:s} {} into data array {}, " \
#                           "cropping/zerospadding instead. Also in the channel dimension." \
#                           "\nDataframe claimed image would be {}".format(filename, im_np.shape, X.shape, df_shape)
                    X_im[:im_np.shape[0], :im_np.shape[1], :im_np.shape[2]] = im_np[:X_im.shape[0], :X_im.shape[1], :X_im.shape[2]]
                except IndexError: # too few indices due to greyscale images
                    im_np = np.array(im)
                    X_im[:im_np.shape[0], :im_np.shape[1], :] = im_np[:h, :w, np.newaxis]
        y = {'genres': np.array(df.filter(regex='genre_'))[:, np.newaxis, np.newaxis, :],
             'styles': np.array(df.filter(regex='style_'))[:, np.newaxis, np.newaxis, :]}
        return X, y

    def train_model(self, pretrained_weights=None):
        print "Training model"
        model = self.create_model()
        if not pretrained_weights is None:
            model.load_weights(pretrained_weights)

        checkpointer = keras.callbacks.ModelCheckpoint(filepath='tmp/weights.hdf5', verbose=1, save_best_only=True)
        model.fit_generator(generator=self.train_generator,
                            steps_per_epoch=100,
                            epochs=10,
                            validation_data=self.val_generator,
                            validation_steps=100,
                            initial_epoch=0,
                            callbacks=[checkpointer]
                            )


if __name__ == '__main__':
    import sys
    pl = PainterLearner(max_batch_size=32, clip_factor=128)
    
    if len(sys.argv) > 1:
        pretrained_weights = sys.argv[1]
    else:
        pretrained_weights = None
    try:
        history = pl.train_model(pretrained_weights=pretrained_weights)
    except:
        pass



